/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt;

import org.junit.Test;

/**
 *
 * @author Yamashita,Takahiro
 */
public class ProcessingEnvironmentUtilTest {

    @Test
    public void コンストラクタカバレッジ() {
        ProcessingEnvironmentUtil util = new ProcessingEnvironmentUtil();
    }

    @Test(expected = AnnotationProcessorException.class)
    public void 必須項目例外() {
        ProcessingEnvironmentUtil util = ProcessingEnvironmentUtil.of(null);
    }

}
