/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.config;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Assert;
import org.junit.Test;
import org.vermeerlab.apt.AnnotationProcessorException;
import org.vermeerlab.apt.command.ProcessorCommandInterface;

/**
 *
 * @author Yamashita,Takahiro
 */
public class CommandConfigScannerHandlerTest {

    public CommandConfigScannerHandlerTest() {
    }

    @Test
    public void カバレッジ対応() {
        CommandConfigScannerHandler commandConfigScannerHandler = new CommandConfigScannerHandler();
    }

    @Test
    public void testCreateCommandInstances_NoCommand() {

        Set<String> commands = Collections.emptySet();
        CommandConfigScannerHandler commandConfigScannerHandler
                                    = new CommandConfigScannerHandler(null, commands);
        Set<ProcessorCommandInterface> instances = commandConfigScannerHandler.createCommandInstances();
        Assert.assertThat(instances.size(), is(0));
    }

    @Test
    public void testCreateCommandInstances() {
        Set<String> commands = new HashSet<>(Arrays.asList("java.lang.String"));
        CommandConfigScannerHandler commandConfigScannerHandler
                                    = new CommandConfigScannerHandler(null, commands);
        try {
            Set<ProcessorCommandInterface> instances = commandConfigScannerHandler.createCommandInstances();
        } catch (AnnotationProcessorException ex) {
            Assert.assertThat(ex.getMessage().contains("Processor Command does not impliment"), is(true));
        }
    }

    @Test
    public void testCreateCommandInstances_ClassNotFoundException() {
        Set<String> commands = new HashSet<>(Arrays.asList("test"));
        CommandConfigScannerHandler commandConfigScannerHandler
                                    = new CommandConfigScannerHandler(null, commands);
        try {
            Set<ProcessorCommandInterface> instances = commandConfigScannerHandler.createCommandInstances();
        } catch (AnnotationProcessorException ex) {
            Assert.assertThat(ex.getMessage().contains("Processor Command can not instance."), is(true));
        }
    }

    @Test
    public void testNewInstanceCommandConfigScanner() {
        Set<String> commands = new HashSet<>(Arrays.asList("test"));
        CommandConfigScannerHandler commandConfigScannerHandler
                                    = new CommandConfigScannerHandler(null, commands);

        class CommandConfigScannerImpl implements CommandConfigScannerInterface {

            CommandConfigScannerImpl() throws InstantiationException {
                throw new InstantiationException();
            }

            @Override
            public CommandConfigScannerInterface readConfig(ConfigXmlReader configXmlReader) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        }

        try {
            CommandConfigScannerInterface scanner
                                          = commandConfigScannerHandler.newInstanceCommandConfigScanner(
                            CommandConfigScannerImpl.class);
        } catch (AnnotationProcessorException ex) {
            Assert.assertThat(ex.getMessage().contains("Processor Command ConfigScanner can not instance."), is(true));
        }
    }
}
