/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic;
import static org.hamcrest.CoreMatchers.is;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.vermeerlab.apt.command.ProcessorCommandManager;

/**
 *
 * @author Yamashita,Takahiro
 */
public class AnnotationProcessorControllerTest {

    private ByteArrayOutputStream _baos;
    private PrintStream _out;

    private ProcessingEnvironment processingEnv;
    private ProcessorCommandManager processorCommandManager;

    @Before
    public void setUp() throws FileNotFoundException {
        _baos = new ByteArrayOutputStream();
        _out = System.out;
        System.setOut(
                new PrintStream(
                        new BufferedOutputStream(_baos)
                )
        );

        this.processingEnv = new ProcessingEnvironment() {
            @Override
            public Map<String, String> getOptions() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public Messager getMessager() {
                return new Messager() {
                    @Override
                    public void printMessage(Diagnostic.Kind kind, CharSequence msg) {
                        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }

                    @Override
                    public void printMessage(Diagnostic.Kind kind, CharSequence msg, Element e) {
                        System.out.println(msg);
                    }

                    @Override
                    public void printMessage(Diagnostic.Kind kind, CharSequence msg, Element e, AnnotationMirror a) {
                        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }

                    @Override
                    public void printMessage(Diagnostic.Kind kind, CharSequence msg, Element e, AnnotationMirror a, AnnotationValue v) {
                        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }

                };
            }

            @Override
            public Filer getFiler() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public Elements getElementUtils() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public Types getTypeUtils() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public SourceVersion getSourceVersion() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public Locale getLocale() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        };

        this.processorCommandManager = ProcessorCommandManager.of("/processor-command.xml");
    }

    @After public void tearDown() {
        System.setOut(_out);
    }

    private String resultValue() {
        String result = _baos.toString();
        return result == null
               ? ""
               : result.replaceAll("\r|\n", "");
    }

    @Test
    public void コンソールメッセージの確認() {
        ProcessorCommandHandler processorCommandHandler
                                = new ProcessorCommandHandler(this.processingEnv, true, this.processorCommandManager);
        /*
        テスト対象のクラスのメソッドをオーバーライドするのは避けるべきだが
        環境情報を取得するためテスト対象のメソッドのテストが実行できないため強制的に書き換えを行う
         */
        AnnotationProcessorController annotationProcessorController = new AnnotationProcessorController() {
            @Override
            public synchronized void init(ProcessingEnvironment processingEnv) {
                super.init(processingEnv);
            }
        };
        annotationProcessorController.init(this.processingEnv);
        annotationProcessorController.processorCommandHandler = processorCommandHandler;

        Set<? extends TypeElement> elements = new HashSet<>();
        annotationProcessorController.process(elements, null);
        System.out.flush();
        Assert.assertThat(resultValue().contains(">>> Annotation Processor Command List >>>"), is(true));
        Assert.assertThat(resultValue().contains("org.vermeerlab.apt.command.test.ProcessorCommand"), is(true));
        Assert.assertThat(resultValue()
                .contains("org.vermeerlab.apt.command.test.ExtentionConfigProcessorCommand"), is(true));
        Assert.assertThat(resultValue().contains("org.vermeerlab.apt.command.test.PostProcessorCommand"), is(true));
        Assert.assertThat(resultValue().contains("<<< Annotation Processor Command List <<<"), is(true));
        Assert.assertThat(resultValue().contains(">>> Annotation Processor Command Config Scanner List >>>"), is(true));
        Assert.assertThat(resultValue()
                .contains("org.vermeerlab.apt.command.test.ExtentionConfigProcessorScanner"), is(true));
        Assert.assertThat(resultValue().contains("<<< Annotation Processor Command Config Scanner List <<<"), is(true));

    }

    @Test
    public void 独自以外の例外捕捉() {
        ProcessorCommandHandler processorCommandHandler
                                = new ProcessorCommandHandler(this.processingEnv, true, this.processorCommandManager) {
            @Override
            public void postProcess() {
                throw new RuntimeException("not custom exception");
            }
        };

        /*
        テスト対象のクラスのメソッドをオーバーライドするのは避けるべきだが
        環境情報を取得するためテスト対象のメソッドのテストが実行できないため強制的に書き換えを行う
         */
        AnnotationProcessorController annotationProcessorController = new AnnotationProcessorController() {
            @Override
            public synchronized void init(ProcessingEnvironment processingEnv) {
                super.init(processingEnv);
            }
        };
        annotationProcessorController.init(this.processingEnv);
        annotationProcessorController.processorCommandHandler = processorCommandHandler;

        Set<? extends TypeElement> elements = new HashSet<>();
        annotationProcessorController.process(elements, null);
        System.out.flush();
        Assert.assertThat(resultValue().contains("not custom exception"), is(true));
    }

    @Test
    public void 検証結果を保持しない場合の例外捕捉() {
        ProcessorCommandHandler processorCommandHandler
                                = new ProcessorCommandHandler(this.processingEnv, true, this.processorCommandManager) {
            @Override
            public void postProcess() {
                throw new AnnotationProcessorException("no ValidationResult");
            }
        };

        /*
        テスト対象のクラスのメソッドをオーバーライドするのは避けるべきだが
        環境情報を取得するためテスト対象のメソッドのテストが実行できないため強制的に書き換えを行う
         */
        AnnotationProcessorController annotationProcessorController = new AnnotationProcessorController() {
            @Override
            public synchronized void init(ProcessingEnvironment processingEnv) {
                super.init(processingEnv);
            }
        };
        annotationProcessorController.init(this.processingEnv);
        annotationProcessorController.processorCommandHandler = processorCommandHandler;

        Set<? extends TypeElement> elements = new HashSet<>();
        annotationProcessorController.process(elements, null);
        System.out.flush();
        Assert.assertThat(resultValue().contains("no ValidationResult"), is(true));
    }

    @Test
    public void 原因例外を保持した例外の補足() {
        ProcessorCommandHandler processorCommandHandler
                                = new ProcessorCommandHandler(this.processingEnv, true, this.processorCommandManager) {
            @Override
            public void postProcess() {
                throw new RuntimeException(new IllegalArgumentException("原因例外です"));
            }
        };

        /*
        テスト対象のクラスのメソッドをオーバーライドするのは避けるべきだが
        環境情報を取得するためテスト対象のメソッドのテストが実行できないため強制的に書き換えを行う
         */
        AnnotationProcessorController annotationProcessorController = new AnnotationProcessorController() {
            @Override
            public synchronized void init(ProcessingEnvironment processingEnv) {
                super.init(processingEnv);
            }
        };
        annotationProcessorController.init(this.processingEnv);
        annotationProcessorController.processorCommandHandler = processorCommandHandler;

        Set<? extends TypeElement> elements = new HashSet<>();
        annotationProcessorController.process(elements, null);
        System.out.flush();
        Assert.assertThat(resultValue().contains("原因例外です"), is(true));
    }

    @Test
    public void noMessageException_カバレッジ対応() {
        AnnotationProcessorController annotationProcessorController = new AnnotationProcessorController() {
            @Override
            public synchronized void init(ProcessingEnvironment processingEnv) {
                super.init(processingEnv);
            }
        };
        annotationProcessorController.printMessage(new AnnotationProcessorException());
    }
}
