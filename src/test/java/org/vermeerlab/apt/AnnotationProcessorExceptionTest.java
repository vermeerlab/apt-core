/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Yamashita,Takahiro
 */
public class AnnotationProcessorExceptionTest {

    @Test
    public void 例外処理のインスタンス化() {
        AnnotationProcessorException ex = new AnnotationProcessorException();
        Assert.assertThat(ex, instanceOf(AnnotationProcessorException.class));
    }

    @Test
    public void 実行時例外を引数にして例外処理のインスタンス化() {
        AnnotationProcessorException ex = new AnnotationProcessorException(new RuntimeException(), "exception message");
        Assert.assertThat(ex.getMessage().replaceAll("\r|\n", ""), is("exception message"));
        Assert.assertThat(ex, instanceOf(AnnotationProcessorException.class));
    }

}
