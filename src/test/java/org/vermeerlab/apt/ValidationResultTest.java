/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt;

import java.util.ArrayList;
import java.util.List;
import javax.tools.Diagnostic;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Yamashita,Takahiro
 */
public class ValidationResultTest {

    public ValidationResultTest() {
    }

    @Test
    public void 検証結果を追記() {
        ValidationResult instance = ValidationResult.create();
        ValidationResult instance2 = ValidationResult.create();
        instance2.append(ValidationResultDetail.of(Diagnostic.Kind.ERROR, "message0", null));
        instance.append(instance2);

        for (int i = 0; i < instance.getDetails().size(); i++) {
            ValidationResultDetail detail = instance.getDetails().get(i);
            Assert.assertThat(detail.getMessage(), is("message" + String.valueOf(i)));
        }
    }

    @Test
    public void getDetailsの対象に全ての区分を設定した場合() {
        ValidationResult instance = ValidationResult.create();
        instance
                .append(ValidationResultDetail.of(Diagnostic.Kind.ERROR, "message0", null))
                .append(ValidationResultDetail.of(Diagnostic.Kind.MANDATORY_WARNING, "message1", null))
                .append(ValidationResultDetail.of(Diagnostic.Kind.WARNING, "message2", null));

        for (int i = 0; i < instance.getDetails().size(); i++) {
            ValidationResultDetail detail = instance.getDetails().get(i);
            Assert.assertThat(detail.getMessage(), is("message" + String.valueOf(i)));
        }
    }

    @Test
    public void getDetailsで区分を指定して詳細情報を取得した場合() {
        ValidationResult instance = ValidationResult.create();
        instance
                .append(ValidationResultDetail.of(Diagnostic.Kind.ERROR, "message0", null))
                .append(ValidationResultDetail.of(Diagnostic.Kind.MANDATORY_WARNING, "message1", null))
                .append(ValidationResultDetail.of(Diagnostic.Kind.WARNING, "message2", null));

        List<ValidationResultDetail> details = instance.getDetails(Diagnostic.Kind.ERROR);
        Assert.assertThat(details.get(0).getMessage(), is("message0"));
    }

    @Test
    public void 必須検証_of_Factoryメソッド() {
        ValidationResultDetail detail = null;
        ValidationResult instance = ValidationResult.of(detail);
        Assert.assertThat(instance.isValid(), is(true));
    }

    @Test
    public void of_で詳細を設定() {
        ValidationResultDetail detail = ValidationResultDetail.of("message");
        ValidationResult instance = ValidationResult.of(detail);
        Assert.assertThat(instance.getAllDetails().size(), is(1));
    }

    @Test
    public void of_で詳細を複数の設定() {
        List<ValidationResultDetail> details = new ArrayList<>();
        ValidationResult instance = ValidationResult.of(details);
        Assert.assertThat(instance.isValid(), is(true));
    }

    @Test
    public void append_nullは無視() {
        ValidationResult instance = ValidationResult.create();
        Assert.assertThat(instance.getAllDetails().size(), is(0));
        ValidationResultDetail detail = null;
        instance.append(detail);
        Assert.assertThat(instance.getAllDetails().size(), is(0));
    }

    @Test
    public void of_ValidationResultを複数指定() {
        ValidationResult result = ValidationResult.of(
                ValidationResult.of(ValidationResultDetail.of("error1")),
                ValidationResult.of(ValidationResultDetail.of("error2"))
        );
        for (int i = 0; i < 1; i++) {
            Assert.assertThat(result.details.get(i).getMessage(), is("error" + String.valueOf(++i)));
        }
    }
}
