/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.command.test;

import java.util.Iterator;
import org.vermeerlab.apt.config.CommandConfigScannerInterface;
import org.vermeerlab.apt.config.ConfigXmlReader;

/**
 * * {@literal AnnotationProcessor} のコマンド実行時に使用する定義情報を管理します.
 *
 * @author Yamashita,Takahiro
 */
public class ExtentionConfigProcessorScanner implements CommandConfigScannerInterface {

    String customtag;

    @Override
    public CommandConfigScannerInterface readConfig(ConfigXmlReader configXmlReader) {
        Iterator<String> list = configXmlReader.scanTextValue("/root/customtag/text()").iterator();
        this.customtag = list.hasNext()
                         ? list.next()
                         : "";
        return this;
    }
}
