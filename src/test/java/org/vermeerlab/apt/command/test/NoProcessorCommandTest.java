/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.command.test;

import com.google.common.io.Resources;
import com.google.common.truth.Truth;
import com.google.testing.compile.JavaFileObjects;
import com.google.testing.compile.JavaSourceSubjectFactory;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import static org.hamcrest.CoreMatchers.is;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.vermeerlab.apt.AnnotationProcessorController;

/**
 *
 * @author Yamashita,Takahiro
 */
public class NoProcessorCommandTest {

    private ByteArrayOutputStream _baos;
    private PrintStream _out;

    @Before
    public void setUp() throws FileNotFoundException {
        _baos = new ByteArrayOutputStream();
        _out = System.out;
        System.setOut(
                new PrintStream(
                        new BufferedOutputStream(_baos)
                )
        );
    }

    @After public void tearDown() {
        System.setOut(_out);
    }

    @Test
    public void 実行コマンドが無い() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/command/test/TestTarget.java"
                )))
                .processedWith(new AnnotationProcessorController(true, "/processor-command_nocommand.xml"))
                .compilesWithoutWarnings();

        System.out.flush();
        String result = _baos.toString();
        String expect = ""
                        + "********* Warning *********\n"
                        + "There is no processor command for `org.vermeerlab.apt.AnnotationProcessorController`.\n"
                        + "If you would not use Pluggable Annotation Processing API with `org.vermeerlab.apt.AnnotationProcessorController`, "
                        + "you can remove `org.vermeerlab.apt.*` dependencies. \n"
                        + "If you want to use `org.vermeerlab.apt.*`. You must make `processor-command.xml` to classpath folder. \n"
                        + "Usage is `https://github.com/vermeerlab/maven`. \n"
                        + "********* Warning *********\n";
        Assert.assertThat(result.contains(expect), is(true));
    }
}
