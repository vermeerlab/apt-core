/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.command.test;

import com.google.common.io.Resources;
import com.google.common.truth.Truth;
import com.google.testing.compile.JavaFileObjects;
import com.google.testing.compile.JavaSourceSubjectFactory;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import static org.hamcrest.CoreMatchers.is;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.vermeerlab.apt.AnnotationProcessorController;

/**
 *
 * @author Yamashita,Takahiro
 */
public class ProcessorCommandTest {

    private ByteArrayOutputStream _baos;
    private PrintStream _out;

    @Before
    public void setUp() throws FileNotFoundException {
        _baos = new ByteArrayOutputStream();
        _out = System.out;
        System.setOut(
                new PrintStream(
                        new BufferedOutputStream(_baos)
                )
        );
    }

    @After public void tearDown() {
        System.setOut(_out);
    }

    private String resultValue() {
        String result = _baos.toString();
        return result == null
               ? ""
               : result.replaceAll("\r|\n", "");
    }

    @Test
    public void 検証のルート確認() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/command/test/TestTarget.java"
                )))
                .processedWith(new AnnotationProcessorController(true))
                .compilesWithoutError();

        System.out.flush();
        String result = resultValue();
        Assert.assertThat(result.contains("org.vermeerlab.apt.command.test"), is(true));
        Assert.assertThat(result.contains(
                "@TestScan()"
                + "public class TestTarget {"
                + "    "
                + "    public TestTarget() {"
                + "        super();"
                + "    }"
                + "}"), is(true));

        Assert.assertThat(result.contains("isSameType=true"), is(true));
        Assert.assertThat(result.contains("TestScan.class count = 1"), is(true));
    }

    @Test(expected = java.lang.AssertionError.class)
    public void 検証異常のルート確認() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/command/test/ErrorTestTarget.java"
                )))
                .processedWith(new AnnotationProcessorController(true))
                .compilesWithoutError();
    }

    @Test
    public void RootCommandConfigScanner_isScanJarFile_False_カバレッジ対応() {
        Truth.assert_()
                .about(JavaSourceSubjectFactory.javaSource())
                .that(JavaFileObjects.forResource(Resources.getResource(
                        "org/vermeerlab/apt/command/test/TestTarget.java"
                )))
                .processedWith(new AnnotationProcessorController(true, "/processor-command_isScanJarFile.xml"))
                .compilesWithoutError();
    }
}
