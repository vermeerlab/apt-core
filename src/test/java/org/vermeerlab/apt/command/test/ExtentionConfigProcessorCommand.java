/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.command.test;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import org.vermeerlab.apt.command.ProcessorCommandInterface;
import org.vermeerlab.apt.config.CommandConfigScannerInterface;
import org.vermeerlab.base.DebugUtil;

/**
 *
 * @author Yamashita,Takahiro
 */
public class ExtentionConfigProcessorCommand implements ProcessorCommandInterface {

    String customtag;

    @Override
    public Class<? extends Annotation> getTargetAnnotation() {
        return org.vermeerlab.apt.command.test.TestScan.class;
    }

    @Override
    public void execute(ProcessingEnvironment processingEnvironment, Element element, Boolean isDebug) {
        DebugUtil debugUtil = new DebugUtil(isDebug);
        debugUtil.print("customtag=" + this.customtag);
    }

    @Override
    public List<Class<? extends CommandConfigScannerInterface>> getCommandConfigScannerClasses() {
        List<Class<? extends CommandConfigScannerInterface>> list = new ArrayList<>();
        list.add(ExtentionConfigProcessorScanner.class);
        return list;
    }

    @Override
    public <T extends CommandConfigScannerInterface> ProcessorCommandInterface setCommandConfigScanners(List<T> commandConfigScanners) {
        ExtentionConfigProcessorScanner scanner = (ExtentionConfigProcessorScanner) commandConfigScanners.iterator().next();
        this.customtag = scanner.customtag;
        return this;
    }

}
