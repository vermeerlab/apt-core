/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.command.test;

import javax.lang.model.element.Element;
import org.vermeerlab.apt.ValidationResult;
import org.vermeerlab.apt.ValidationResultDetail;
import org.vermeerlab.apt.command.ValidationInterface;

/**
 *
 * @author Yamashita,Takahiro
 */
public class TestScanValidation implements ValidationInterface {

    private final Element element;

    private TestScanValidation(Element element) {
        this.element = element;
    }

    public static TestScanValidation of(Element element) {
        return new TestScanValidation(element);
    }

    @Override
    public ValidationResult validate() {
        ValidationResult result = ValidationResult.create();
        if (this.element.getSimpleName().toString().equals("ErrorTestTarget")) {
            result.append(ValidationResult.of(ValidationResultDetail.of("TestScanValidationError")));
        }
        return result;
    }
}
