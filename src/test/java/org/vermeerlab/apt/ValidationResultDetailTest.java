/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt;

import java.util.Optional;
import javax.tools.Diagnostic;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Yamashita,Takahiro
 */
public class ValidationResultDetailTest {

    @Test
    public void エラー以外の区分の場合は検証結果は無効な値() {
        ValidationResultDetail instance = ValidationResultDetail.of("Test");
        Optional<String> message = instance.getMessage(Diagnostic.Kind.NOTE);
        Assert.assertThat(message.isPresent(), is(false));

        boolean isExistsElement = instance.getElement().isPresent();
        Assert.assertThat(isExistsElement, is(false));
    }

    @Test
    public void デフォルトの結果詳細はエラー() {
        ValidationResultDetail instance = ValidationResultDetail.of("Test");
        Assert.assertEquals(instance.getKind(), Diagnostic.Kind.ERROR);
    }

    @Test
    public void ElementがNullでも検証結果詳細の構築は可能() {
        ValidationResultDetail instance = ValidationResultDetail.of("Test", null);
        Assert.assertEquals(instance.getKind(), Diagnostic.Kind.ERROR);
    }

    @Test
    public void 必須検証_of() {
        try {
            ValidationResultDetail.of(null, null, null);
        } catch (AnnotationProcessorException ex) {
            Assert.assertThat(ex.getMessage(), is("kind and message is required for AnnotationProcessorException."));
        }
    }

    @Test
    public void コンストラクタカバレッジ() {
        ValidationResultDetail detail = new ValidationResultDetail();
    }

}
