/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt;

import java.lang.annotation.Annotation;
import java.util.Set;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import org.vermeerlab.apt.command.ProcessorCommandInterface;
import org.vermeerlab.apt.command.ProcessorCommandManager;

/**
 * 処理対象コマンドインターフェースを実装したクラスを操作します.
 *
 * @see org.vermeerlab.apt.command.ProcessorCommandInterface
 * @see org.vermeerlab.apt.command.PostProcessorCommandInterface
 * @author Yamashita,Takahiro
 */
public class ProcessorCommandHandler {

    final ProcessorCommandManager processorCommandManager;
    final ProcessingEnvironment processingEnv;
    final Boolean isDubug;

    /**
     * for expansion
     */
    protected ProcessorCommandHandler() {
        this(null, null, null);
    }

    protected ProcessorCommandHandler(ProcessingEnvironment processingEnv, Boolean isDebug, ProcessorCommandManager processorCommandManager) {
        this.processingEnv = processingEnv;
        this.isDubug = isDebug;
        this.processorCommandManager = processorCommandManager;
    }

    /**
     * インスタンスを構築します.
     *
     * @param processingEnv {@literal AnnotationProcessor} 実行時の環境情報
     * @param isDebug デバッグ区分
     * @param processorCommandXmlPath Processor実行環境の情報のパス
     * @return 構築したインスタンス
     */
    public static ProcessorCommandHandler of(ProcessingEnvironment processingEnv, Boolean isDebug, String processorCommandXmlPath) {
        return new ProcessorCommandHandler(processingEnv, isDebug, ProcessorCommandManager.of(processorCommandXmlPath));
    }

    /**
     * コマンドインターフェースを実装したクラスを順次実行します.
     *
     * @param elements {@literal AnnotationProcessor} で取得した要素
     * @param roundEnv {@literal AnnotationProcessor} で取得した実行時のラウンド情報
     * @throws AnnotationProcessorException 検証結果に不正があった場合
     *
     * @see org.vermeerlab.apt.command.ProcessorCommandInterface
     * @see org.vermeerlab.apt.command.PostProcessorCommandInterface
     */
    public void execute(Set<? extends TypeElement> elements, RoundEnvironment roundEnv) {
        elements.stream()
                .map(typeElement -> roundEnv.getElementsAnnotatedWith(typeElement))
                .flatMap(Set::stream)
                .forEach(element -> {
                    this.processorCommandManager.commands().stream()
                            .filter(command -> this.filter(command, element))
                            .forEach(command -> command.execute(this.processingEnv, element, this.isDubug));
                });
    }

    /**
     * {@code Element} が処理対象か判定します.
     *
     * @param command 実行コマンド
     * @param element 処理対象を抽出元となる{@link javax.lang.model.element.Element}
     * @return 処理対象であった場合、true
     */
    boolean filter(ProcessorCommandInterface command, Element element) {
        Class<? extends Annotation> targetAnnotation = command.getTargetAnnotation();
        return targetAnnotation == null
               ? true
               : element.getAnnotation(targetAnnotation) != null;
    }

    /**
     * 最終ラウンドにコマンドを実行します.
     *
     * @see org.vermeerlab.apt.command.PostProcessorCommandInterface
     */
    public void postProcess() {
        this.processorCommandManager.postProcess().stream()
                .forEach(command -> command.postProcess(this.processingEnv, this.isDubug));
    }
}
