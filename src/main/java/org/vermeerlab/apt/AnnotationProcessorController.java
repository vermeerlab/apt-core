package org.vermeerlab.apt;

/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
import java.util.Arrays;
import java.util.Set;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;

/**
 * AnnotationProcessorを実行します.
 * <P>
 * 本クラスを{@literal src/main/resources/META-INF/services/javax.annotation.processing.Processor}に記述して
 * サービスとして登録します.
 *
 * @author Yamashita,Takahiro
 */
@SupportedSourceVersion(SourceVersion.RELEASE_8)
@SupportedAnnotationTypes({"*"})
public class AnnotationProcessorController extends AbstractProcessor {

    private final Boolean isDebug;
    private String processorCommandXmlPath;
    ProcessorCommandHandler processorCommandHandler;

    /**
     * {@literal Annotation Processor} から実行されるインスタンスを構築します.
     * <P>
     * デフォルトのデバッグ区分は {@code false} です.
     */
    public AnnotationProcessorController() {
        this(false);
    }

    /**
     * デバッグ区分を指定してインスタンスを構築します.
     * <P>
     * テストユニットにて生成コードの確認をしたい場合に使用することを想定しています.
     *
     * @param isDebug デバッグ区分
     */
    public AnnotationProcessorController(Boolean isDebug) {
        this(isDebug, System.getProperty("processorcommand.classpath"));
    }

    /**
     * デバッグ区分を指定してインスタンスを構築します.
     * <P>
     * テストユニットにて生成コードの確認をしたい場合に使用することを想定しています.<br>
     * システムプロパティではなく、直接 定義用Xmlを指定できます.
     *
     * @param isDebug デバッグ区分
     * @param processorCommandXml Processor実行環境の情報
     */
    public AnnotationProcessorController(Boolean isDebug, String processorCommandXml) {
        this.isDebug = isDebug;
        this.processorCommandXmlPath = processorCommandXml;
    }

    /**
     * 初期化をします.
     *
     * @param processingEnv 環境情報
     */
    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        this.processorCommandHandler = ProcessorCommandHandler.of(processingEnv, this.isDebug,
                                                                  this.processorCommandXmlPath);
    }

    /**
     * {@literal AnnotationProcessor} によりクラス生成や検証を行います.
     * <P>
     * 処理対象はアノテーションを付与した要素のみです.
     *
     * @param elements {@link  javax.annotation.processing.SupportedAnnotationTypes} で指定することにより処理対象となった注釈がされている要素
     * @param roundEnv 注釈処理ラウンド
     * @return 生成が正常終了した場合は {@code true}.
     */
    @Override
    public boolean process(Set<? extends TypeElement> elements, RoundEnvironment roundEnv) {
        try {
            if (elements.isEmpty()) {
                this.processorCommandHandler.postProcess();
                System.clearProperty("processorcommand.classpath");
            } else {
                this.processorCommandHandler.execute(elements, roundEnv);
            }
        } catch (AnnotationProcessorException ex) {
            this.printMessage(ex);
            return true;
        } catch (Exception ex) {
            String message = ex.getMessage() + Arrays.toString(ex.getStackTrace());
            if (ex.getCause() != null) {
                message = message
                          + System.lineSeparator()
                          + "****cause****:"
                          + ex.getCause().toString() + ":" + ex.getCause().getMessage() + ":"
                          + System.lineSeparator()
                          + Arrays.toString(ex.getCause().getStackTrace());
            }
            this.printMessage(Diagnostic.Kind.ERROR, message, null);
            return false;
        }
        return true;
    }

    /**
     * メッセージを出力します.
     *
     * @param kind メッセージ出力区分
     * @param message 出力するメッセージ
     * @param element 出力対象のElement.メッセージのヒントに使用します.対象Elementが無い場合は{@code null}を指定してください.
     */
    void printMessage(Diagnostic.Kind kind, String message, Element element) {
        this.processingEnv.getMessager().printMessage(kind, message, element);
    }

    /**
     * AnnotationProcessor処理中の独自例外のメッセージを出力します.
     * <P>
     * 例外に検証結果 {@link ValidationResult} を設定している場合、
     * 例外のメッセージと併せて 検証結果の詳細メッセージを出力します.
     *
     * @param ex AnnotationProcessorの独自例外
     */
    void printMessage(AnnotationProcessorException ex) {
        String _rootResult = ex.getMessage() == null || ex.getMessage().isEmpty()
                             ? ""
                             : "【" + ex.getMessage() + "】";

        if (ex.getValidationResult().isPresent()) {
            ex.getValidationResult().ifPresent(
                    result -> result.getAllDetails().stream()
                            .forEach(detail -> {
                                Element element = detail.getElement().isPresent()
                                                  ? detail.getElement().get() : null;
                                this.printMessage(detail.getKind(), _rootResult + detail.getMessage(), element);
                            })
            );
            return;
        }

        if (_rootResult.isEmpty()) {
            return;
        }
        this.printMessage(Diagnostic.Kind.ERROR, _rootResult, null);
    }
}
