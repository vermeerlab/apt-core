/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.config;

import java.util.Collections;
import java.util.List;
import org.vermeerlab.xml.XmlReader;

/**
 * 定義用Xmlを読み込んだ定義情報を管理する機能を提供します.
 *
 * @author Yamashita,Takahiro
 */
public class ConfigXmlReader {

    XmlReader xmlReader;

    /**
     * for expansion
     */
    protected ConfigXmlReader() {
        this(null);
    }

    protected ConfigXmlReader(XmlReader xmlReader) {
        this.xmlReader = xmlReader;
    }

    /**
     * インスタンスを構築します.
     * <P>
     *
     * @param processorCommandXmlPath 定義用Xmlのパス
     * @return 構築したインスタンス
     */
    public static ConfigXmlReader of(String processorCommandXmlPath) {
        if (processorCommandXmlPath == null) {
            return new ConfigXmlReader(null);
        }
        XmlReader xmlReader = XmlReader.ofClassPath(processorCommandXmlPath);
        return new ConfigXmlReader(xmlReader);
    }

    /**
     * xpath指定により要素値を取得して返却します.
     * <P>
     * 要素が無い、および 要素の値が空文字も場合はリストの対象外とします.
     *
     * @param conditionXpath 検索条件のxpath記述
     * @return 取得した要素値リスト
     */
    public List<String> scanTextValue(String conditionXpath) {
        return this.xmlReader == null
               ? Collections.emptyList()
               : this.xmlReader.scanTextValue(conditionXpath);
    }

    /**
     * xpath指定したノードが存在するか判定します.
     *
     * @param conditionXpath 検索条件のxpath記述
     * @return ノードが存在する場合 true
     */
    public Boolean hasNode(String conditionXpath) {
        return this.xmlReader == null
               ? Boolean.FALSE
               : this.xmlReader.hasNode(conditionXpath);
    }

}
