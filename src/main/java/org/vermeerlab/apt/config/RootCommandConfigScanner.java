/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.config;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.vermeerlab.apt.AnnotationProcessorException;
import org.vermeerlab.apt.command.ProcessorCommandInterface;
import org.vermeerlab.scanner.ImplementClasses;

/**
 * 定義用Xmlのルート情報を取得する機能を提供します.
 *
 * @author Yamashita,Takahiro
 */
public class RootCommandConfigScanner implements CommandConfigScannerInterface {

    RootCommandConfig rootCommandConfig;

    public RootCommandConfigScanner() {
        this.rootCommandConfig = new RootCommandConfig();
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public RootCommandConfigScanner readConfig(ConfigXmlReader configXmlReader) {
        Iterator<String> displayCommandList = configXmlReader.scanTextValue("/root/displayCommandList/text()").iterator();
        boolean isDisplay = displayCommandList.hasNext()
                            ? Boolean.valueOf(displayCommandList.next())
                            : false;

        List<String> excludeCommands = configXmlReader.scanTextValue("/root/excludeCommands/excludeCommand/text()");

        this.rootCommandConfig = new RootCommandConfig(isDisplay, excludeCommands);
        return this;
    }

    /**
     * 有効なコマンドクラスの完全修飾名リストを返却します.
     *
     * @return 有効なコマンドクラスの完全修飾名リスト
     * @throws AnnotationProcessorException 有効コマンドが０件の場合
     */
    public Set<String> getValidCommandNames() {
        Set<String> excludeCommandAggregates = new HashSet<>();
        excludeCommandAggregates.addAll(this.rootCommandConfig.excludeCommands);

        Set<String> includeCommandAggregates = ImplementClasses.of(ProcessorCommandInterface.class).get();

        Set<String> commands = includeCommandAggregates.stream()
                .filter(include -> excludeCommandAggregates.contains(include) == false)
                .collect(Collectors.toSet());

        this.postCondition(commands);
        return commands;
    }

    /**
     * 有効なコマンドクラスの完全修飾名をコンソールに出力します.
     *
     * @param commands 実行コマンドのインスタンス
     */
    public void displayCommandInfo(Set<ProcessorCommandInterface> commands) {
        if (this.rootCommandConfig.isDisplay == false) {
            return;
        }

        System.out.println(">>> Annotation Processor Command List >>>");
        commands.stream()
                .map(command -> command.getClass().getCanonicalName())
                .forEach(System.out::println);
        System.out.println("<<< Annotation Processor Command List <<<");

        System.out.println(">>> Annotation Processor Command Config Scanner List >>>");
        commands.stream()
                .flatMap(command -> command.getCommandConfigScannerClasses().stream())
                .map(scanner -> scanner.getCanonicalName())
                .forEach(System.out::println);
        System.out.println("<<< Annotation Processor Command Config Scanner List <<<");

    }

    //
    void postCondition(Set<String> commands) {
        if (commands.isEmpty()) {
            String message = ""
                             + "********* Warning *********\n"
                             + "There is no processor command for `org.vermeerlab.apt.AnnotationProcessorController`.\n"
                             + "If you would not use Pluggable Annotation Processing API with `org.vermeerlab.apt.AnnotationProcessorController`, "
                             + "you can remove `org.vermeerlab.apt.*` dependencies. \n"
                             + "If you want to use `org.vermeerlab.apt.*`. You must make `processor-command.xml` to classpath folder. \n"
                             + "Usage is `https://github.com/vermeerlab/maven`. \n"
                             + "********* Warning *********\n";
            System.out.println(message);
        }
    }
}
