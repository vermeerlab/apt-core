/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.config;

import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.vermeerlab.apt.AnnotationProcessorException;
import org.vermeerlab.apt.command.ProcessorCommandInterface;

/**
 * ProcessorCommandの定義情報を検索します.
 *
 * @author Yamashita,Takahiro
 */
public class CommandConfigScannerHandler {

    final ConfigXmlReader configXmlReader;
    final Set<String> commandNames;

    /**
     * For expansion
     */
    protected CommandConfigScannerHandler() {
        this(null, null);
    }

    protected CommandConfigScannerHandler(ConfigXmlReader configXmlReader, Set<String> commandNames) {
        this.configXmlReader = configXmlReader;
        this.commandNames = commandNames;
    }

    /**
     * インスタンスを構築します.
     *
     * @param configXmlReader 定義用xml情報の読み込みを管理するクラス
     * @param commandNames 有効なコマンドクラスの完全修飾名リスト
     * @return 構築したインスタンス
     */
    public static CommandConfigScannerHandler of(ConfigXmlReader configXmlReader, Set<String> commandNames) {
        return new CommandConfigScannerHandler(configXmlReader, commandNames);
    }

    /**
     * コマンドのクラスパスからクラスをインスタンス化します.
     *
     * @return 構築したコマンドのインスタンスリスト
     * @throws AnnotationProcessorException {@link ProcessorCommandInterface }
     * の実装で無いクラスを指定した場合、コマンドクラスのインスタンス構築時に異常終了した場合
     */
    public Set<ProcessorCommandInterface> createCommandInstances() {
        Set<ProcessorCommandInterface> commands = new HashSet<>();
        if (commandNames.isEmpty()) {
            return commands;
        }
        try {
            for (String path : commandNames) {
                Class<?> clazz = Class.forName(path);
                Object instance = clazz.getDeclaredConstructor().newInstance();
                if (instance instanceof ProcessorCommandInterface == false) {
                    throw new AnnotationProcessorException(
                            "Processor Command does not impliment ", ProcessorCommandInterface.class.getCanonicalName()
                    );
                }
                ProcessorCommandInterface command = (ProcessorCommandInterface) instance;
                this.scanConfigXml(command);
                commands.add(command);
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | SecurityException | IllegalArgumentException | InvocationTargetException ex) {
            throw new AnnotationProcessorException(ex, "Processor Command can not instance.");
        }
        return commands;
    }

    /**
     * 定義用Xmlの情報をコマンドインスタンスに付与します.
     *
     * @param command コマンドのインスタンス
     */
    void scanConfigXml(ProcessorCommandInterface command) {
        List<CommandConfigScannerInterface> scannerList = command.getCommandConfigScannerClasses().stream()
                .map(this::newInstanceCommandConfigScanner)
                .map(scanner -> scanner.readConfig(configXmlReader))
                .collect(Collectors.toList());
        command.setCommandConfigScanners(scannerList);
    }

    /**
     * 定義用Xmlの情報を保持したクラスのインスタンスを返却します.
     *
     * @param configClassPath
     * @return 構築したインスタンス
     */
    CommandConfigScannerInterface newInstanceCommandConfigScanner(Class<?> clazz) {
        try {
            Object instance = clazz.getDeclaredConstructor().newInstance();
            return (CommandConfigScannerInterface) instance;
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException ex) {
            throw new AnnotationProcessorException(ex, "Processor Command ConfigScanner can not instance.");
        }
    }
}
