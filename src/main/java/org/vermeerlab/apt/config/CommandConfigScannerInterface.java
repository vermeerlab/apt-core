/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.config;

/**
 * {@literal AnnotationProcessor} のコマンド実行時に使用する定義情報を取得する機能に用いるインターフェースです.
 * <P>
 * コマンドで管理する情報が増えた場合には、本インターフェースを用いて統一的な処理を行うことを想定しています.
 *
 * @author Yamashita,Takahiro
 */
public interface CommandConfigScannerInterface {

    /**
     * コマンド定義から情報を取得・保持したインスタンスを返却します.
     *
     * @param configXmlReader 定義情報のXML情報
     * @return 情報を取得・保持したの具象クラス
     */
    public CommandConfigScannerInterface readConfig(ConfigXmlReader configXmlReader);

}
