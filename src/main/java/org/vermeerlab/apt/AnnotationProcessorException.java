/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt;

import java.util.Optional;
import org.vermeerlab.base.AbstractCustomException;

/**
 * {@literal AnnotationProcessor}による処理中に発生した例外です.
 * <P>
 * 検証結果をインスタンス時に設定しておくことで、メッセージ出力時に詳細の情報を出力させることができます.
 *
 * @author Yamashita,Takahiro
 */
public class AnnotationProcessorException extends AbstractCustomException {

    private static final long serialVersionUID = 1L;
    private transient ValidationResult validationResult;

    /**
     * 実行時例外を構築します.
     */
    public AnnotationProcessorException() {
    }

    /**
     * 実行時例外を構築します.
     *
     * @param messages 例外情報に付与する文字列
     */
    public AnnotationProcessorException(String... messages) {
        super(messages);
    }

    /**
     * メッセージに加えて実行時例外情報のスタックトレース情報を付与した実行時例外を構築します.
     * <P>
     * @param ex 例外
     * @param messages 例外情報に付与する文字列
     */
    public AnnotationProcessorException(Exception ex, String... messages) {
        super(ex, messages);
    }

    /**
     * AnnotationProcessorにおける検証結果を保持した実行時例外を作成します.
     *
     * @param validationResult 検証結果
     * @param messages 出力メッセージ
     */
    public AnnotationProcessorException(ValidationResult validationResult, String... messages) {
        super(messages);
        this.validationResult = validationResult;
    }

    /**
     * 例外発生時の検証結果を返却します.
     * <P>
     * 検証以外で発生した例外では検証結果を設定していないため {@code null} を返却します.
     *
     * @return 検証結果.検証以外の例外の場合は {@code null} を返却します.
     */
    public Optional<ValidationResult> getValidationResult() {
        return Optional.ofNullable(this.validationResult);
    }
}
