/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;
import javax.tools.Diagnostic;

/**
 * 検証結果を管理します.
 *
 * @author Yamashita,Takahiro
 */
public class ValidationResult {

    List<ValidationResultDetail> details;

    /**
     * For expansion
     */
    protected ValidationResult() {
        this.details = new CopyOnWriteArrayList<>();
    }

    /**
     * インスタンスを構築します
     *
     * @return 構築したインスタンス
     */
    public static ValidationResult create() {
        return new ValidationResult();
    }

    /**
     * 別の検証結果の詳細をマージしたインスタンスを新たに構築します.
     *
     * @param result 検証結果
     * @return 検証結果の詳細をマージした、新たなインスタンス
     */
    public static ValidationResult of(ValidationResult... result) {
        ValidationResult _result = ValidationResult.create();
        for (ValidationResult item : result) {
            _result.append(item.getAllDetails());
        }
        return _result;
    }

    /**
     * 検証結果の詳細を追記したインスタンスを構築します.
     * <P>
     * {@code detail}が {@code null} の場合は、詳細情報無効として保持対象外とします（実行時例外は発生しません）.
     *
     * @param detail 検証結果詳細の情報
     * @return 構築したインスタンス
     */
    public static ValidationResult of(ValidationResultDetail detail) {
        ValidationResult result = ValidationResult.create();
        if (detail == null) {
            return result;
        }
        result.append(detail);
        return result;
    }

    /**
     * 検証結果の詳細を追記したインスタンスを構築します.
     *
     * @param details 検証結果詳細の情報リスト
     * @return 構築したインスタンス
     */
    public static ValidationResult of(List<ValidationResultDetail> details) {
        ValidationResult result = ValidationResult.create();
        result.append(details);
        return result;
    }

    /**
     * 検証結果の詳細（エラーおよび警告関連）を返却します.
     * <P>
     * 返却対象は
     * {@link javax.tools.Diagnostic.Kind#ERROR}、
     * {@link javax.tools.Diagnostic.Kind#MANDATORY_WARNING}、
     * {@link javax.tools.Diagnostic.Kind#WARNING}です.
     *
     * @return 検証結果の詳細リスト.
     */
    public List<ValidationResultDetail> getDetails() {
        return this.details.stream()
                .filter(detail -> {
                    return detail.getMessage(Diagnostic.Kind.ERROR).isPresent()
                           || detail.getMessage(Diagnostic.Kind.MANDATORY_WARNING).isPresent()
                           || detail.getMessage(Diagnostic.Kind.WARNING).isPresent();
                })
                .collect(Collectors.toList());
    }

    /**
     * 指定した区分の検証結果の詳細を返却します.
     *
     * @param kind 検証結果から取得をしたい対象の区分
     * @return 検証結果の詳細リスト.
     */
    public List<ValidationResultDetail> getDetails(Diagnostic.Kind kind) {
        return this.details.stream()
                .filter(detail -> detail.getMessage(kind) != null)
                .collect(Collectors.toList());
    }

    /**
     * 検証結果の詳細を全て返却します.
     *
     * @return 検証結果の詳細リスト.
     */
    public List<ValidationResultDetail> getAllDetails() {
        return Collections.unmodifiableList(this.details);
    }

    /**
     * 検証結果の詳細を追記したインスタンスを返却します.
     *
     * @param result 検証結果
     * @return 検証結果の詳細を追記したインスタンス
     */
    public ValidationResult append(ValidationResult... result) {
        for (ValidationResult item : result) {
            this.append(item.getAllDetails());
        }
        return this;
    }

    /**
     * 検証結果の詳細を追記したインスタンスを返却します.
     * <P>
     * {@code detail} がnullの場合は、詳細情報無効として保持対象外とします.
     *
     * @param detail 検証結果詳細の情報
     * @return 検証結果の詳細を追記したインスタンス
     */
    public ValidationResult append(ValidationResultDetail detail) {
        if (detail == null) {
            return this;
        }
        this.details.add(detail);
        return this;
    }

    /**
     * 検証結果の詳細を追記したインスタンスを返却します.
     *
     * @param details 検証結果詳細の情報リスト
     * @return 検証結果の詳細を追記したインスタンス
     */
    public ValidationResult append(List<ValidationResultDetail> details) {
        for (ValidationResultDetail detail : details) {
            this.append(detail);
        }
        return this;
    }

    /**
     * 検証判定（異常なし）を返却します.
     *
     * @return 検証結果に
     * {@link javax.tools.Diagnostic.Kind#ERROR}、
     * {@link javax.tools.Diagnostic.Kind#MANDATORY_WARNING}、
     * {@link javax.tools.Diagnostic.Kind#WARNING}を含まない場合は、true
     */
    public Boolean isValid() {
        return this.getDetails().isEmpty();
    }
}
