/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright ? 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt;

import java.util.Optional;
import javax.lang.model.element.Element;
import javax.tools.Diagnostic;

/**
 * 検証結果の詳細情報を管理します.
 *
 * @author Yamashita,Takahiro
 */
public class ValidationResultDetail {

    final Diagnostic.Kind kind;
    final String message;
    final Element element;

    /**
     * for expansion
     */
    protected ValidationResultDetail() {
        this.kind = null;
        this.message = null;
        this.element = null;
    }

    protected ValidationResultDetail(Diagnostic.Kind kind, String message, Element element) {
        this.kind = kind;
        this.message = message;
        this.element = element;
    }

    /**
     * インスタンスを構築します.
     *
     * @param kind 検証結果区分
     * @param message 検証結果メッセージ
     * @param element 検証対象の結果を出力する要素.指定しない場合は {@code null} を設定してください.
     * @return 構築したインスタンス
     * @throws AnnotationProcessorException kind または messageが{@code null}の場合
     */
    public static ValidationResultDetail of(Diagnostic.Kind kind, String message, Element element) {
        if (kind == null || message == null || message.equals("")) {
            throw new AnnotationProcessorException("kind and message is required for AnnotationProcessorException.");
        }
        return new ValidationResultDetail(kind, message, element);
    }

    /**
     * インスタンスを構築します.
     * <P>
     * 基本的に検証結果については不正検証を行うことを想定し、デフォルトを{@link  javax.tools.Diagnostic.Kind#ERROR}としています.
     *
     * @param message 検証結果メッセージ
     * @param element 検証対象の結果を出力する要素.指定しない場合は, {@code null} を設定してください.
     * @return 構築したインスタンス.区分は{@link javax.tools.Diagnostic.Kind#ERROR}です.
     * @throws AnnotationProcessorException messageが{@code null}の場合
     */
    public static ValidationResultDetail of(String message, Element element) {
        return ValidationResultDetail.of(Diagnostic.Kind.ERROR, message, element);
    }

    /**
     * インスタンスを構築します.
     * <P>
     * 基本的に検証結果については不正検証を行うことを想定し、デフォルトを{@link  javax.tools.Diagnostic.Kind#ERROR}としています.
     *
     * @param message 検証結果メッセージ
     * @return 構築したインスタンス.区分は{@link javax.tools.Diagnostic.Kind#ERROR}です.
     * @throws AnnotationProcessorException messageが{@code null}の場合
     */
    public static ValidationResultDetail of(String message) {
        return ValidationResultDetail.of(Diagnostic.Kind.ERROR, message, null);
    }

    /**
     * 検証結果詳細の区分を返却します.
     *
     * @return 検証結果の詳細区分
     */
    public Diagnostic.Kind getKind() {
        return this.kind;
    }

    /**
     * 検証結果詳細メッセージを返却します.
     *
     * @return 詳細メッセージ
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * 検証結果詳細の対象要素を返却します.
     *
     * @return 検証時に設定した対象要素を返却します.任意指定のため検証時に未設定（{@code null}）の場合がありえます.
     */
    public Optional<Element> getElement() {
        return Optional.ofNullable(this.element);
    }

    /**
     * 区分を指定して詳細メッセージを返却します.
     * <P>
     * 区分に合致していない場合は {@code null} を返却します.
     *
     * @param kind 検証結果区分
     * @return 区分に合致した場合、保持しているメッセージを返却します.不一致の場合は {@code null} を返却します.
     */
    Optional<String> getMessage(Diagnostic.Kind kind) {
        if (this.kind == kind) {
            return Optional.ofNullable(this.message);
        }
        return Optional.empty();
    }
}
