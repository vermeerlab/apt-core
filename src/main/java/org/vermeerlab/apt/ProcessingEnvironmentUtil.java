/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt;

import com.sun.source.tree.Tree;
import com.sun.source.util.Trees;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.type.TypeMirror;

/**
 * {@link javax.annotation.processing.ProcessingEnvironment} を用いた操作を集約させるためのユーティリティです.
 * <P>
 * 環境に関する実装を集約させておくことで、環境に依存する実装の発散を防ぐことを目的としたクラスです.
 * 使用するメソッドの単純な委譲や、目的に応じた簡単なラッパーメソッドを準備することで実装補助を行います.
 *
 * @author Yamashita,Takahiro
 */
public class ProcessingEnvironmentUtil {

    final ProcessingEnvironment processingEnv;

    /**
     * for expansion
     */
    protected ProcessingEnvironmentUtil() {
        this(null);
    }

    protected ProcessingEnvironmentUtil(ProcessingEnvironment processingEnv) {
        this.processingEnv = processingEnv;
    }

    /**
     * インスタンスを構築します.
     *
     * @param processingEnv {@literal AnnotationProcessor} の実行環境情報
     * @return 構築したインスタンス
     * @throws AnnotationProcessorException processingEnvが{@code null}の場合
     */
    public static ProcessingEnvironmentUtil of(ProcessingEnvironment processingEnv) {
        if (processingEnv == null) {
            throw new AnnotationProcessorException("ProcessingEnvironment is null. it must set. ");
        }
        return new ProcessingEnvironmentUtil(processingEnv);
    }

    /**
     * Elementのパッケージ名を取得して返却します.
     *
     * @param element 対象のElement
     * @return パッケージ名
     */
    public String getPackagePath(Element element) {
        return this.processingEnv.getElementUtils().getPackageOf(element).getQualifiedName().toString();
    }

    /**
     * 要素詳細のツリー情報を返却します.
     * <P>
     * 呼出側で用途に応じてキャストをしてください.
     *
     * @param element 対象のElement
     * @return 保持要素をルートとした詳細ツリー情報
     */
    public Tree getTree(Element element) {
        return Trees.instance(this.processingEnv).getTree(element);
    }

    /**
     * Elementの型が等しい、もしくは継承しているか判定します.
     * <P>
     * 継承元も含めて検証を行います.
     *
     * @param typeMirror 検証対象の型
     * @param clazz 検証する型
     * @return 等しい、もしくは継承をしている場合 true
     */
    public Boolean isSameType(TypeMirror typeMirror, Class<?> clazz) {
        List<TypeMirror> types = new ArrayList<>();
        List<? extends TypeMirror> superTypes = this.processingEnv.getTypeUtils().directSupertypes(typeMirror);
        types.addAll(superTypes);
        types.add(typeMirror);
        TypeMirror clazzType = this.processingEnv.getElementUtils().getTypeElement(clazz.getCanonicalName()).asType();
        return types.stream().anyMatch(type
                -> this.processingEnv.getTypeUtils().isSameType(type, clazzType)
        );
    }
}
