/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.command;

import javax.annotation.processing.ProcessingEnvironment;

/**
 * {@literal Annotation Processor}の最終ラウンドで実行するコマンドのインターフェースです.
 * <P>
 * ラウンド毎に実行する処理は
 * {@link org.vermeerlab.apt.command.ProcessorCommandInterface} の実装を実行します.
 *
 * @see ProcessorCommandInterface
 * @author Yamashita,Takahiro
 */
public interface PostProcessorCommandInterface extends ProcessorCommandInterface {

    /**
     * {@literal Annotation Processor}の最終ラウンドでコマンドを実行します.
     *
     * @param processingEnvironment {@literal AnnotationProcassor} 実行時の環境情報
     * @param isDebug デバッグ区分
     */
    public void postProcess(ProcessingEnvironment processingEnvironment, Boolean isDebug);

}
