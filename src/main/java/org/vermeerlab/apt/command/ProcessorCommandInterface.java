/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.command;

import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.List;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import org.vermeerlab.apt.config.CommandConfigScannerInterface;

/**
 * {@literal Annotation Processor}のラウンド毎に実行するコマンドのインターフェースです.
 *
 * @author Yamashita,Takahiro
 */
public interface ProcessorCommandInterface {

    /**
     * 処理対象のアノテーションを返却します.
     *
     * @return 処理対象のアノテーション
     */
    public Class<? extends Annotation> getTargetAnnotation();

    /**
     * {@literal Annotation Processor}のラウンド毎にコマンドを実行します.
     *
     * @param processingEnvironment {@literal AnnotationProcassor} 実行時の環境情報
     * @param element 処理対象のElement
     * @param isDebug デバッグ区分
     */
    public void execute(ProcessingEnvironment processingEnvironment, Element element, Boolean isDebug);

    /**
     * コマンド実行に使用する定義情報の管理クラスを返却します.
     *
     * @return 定義情報の管理クラス
     */
    public default List<Class<? extends CommandConfigScannerInterface>> getCommandConfigScannerClasses() {
        return Collections.emptyList();
    }

    /**
     * コマンド実行に使用する定義情報の管理クラスを設定します.
     *
     * @param <T> コマンド定義管理情報の型
     * @param commandConfigScanners コマンド定義情報
     * @return 定義情報を設定したコマンドインスタンス
     */
    public default <T extends CommandConfigScannerInterface> ProcessorCommandInterface setCommandConfigScanners(List<T> commandConfigScanners) {
        return this;
    }

}
