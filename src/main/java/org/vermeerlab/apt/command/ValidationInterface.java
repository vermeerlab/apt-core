/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.command;

import org.vermeerlab.apt.ValidationResult;

/**
 * {@link CommandValidator} にて統一的に検証を行うためのインターフェースです.
 *
 * @author Yamashita,Takahiro
 */
public interface ValidationInterface {

    /**
     * 検証を実行し検証結果を返却します.
     *
     * @return 検証結果
     */
    public ValidationResult validate();
}
