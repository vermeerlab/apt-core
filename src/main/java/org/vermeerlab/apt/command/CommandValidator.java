/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.command;

import java.lang.annotation.Annotation;
import org.vermeerlab.apt.AnnotationProcessorException;
import org.vermeerlab.apt.ValidationResult;

/**
 * {@link ValidationInterface} の実装を対象に検証を行います.
 *
 * @author Yamashita,Takahiro
 */
public class CommandValidator {

    final Class<? extends Annotation> rootAnnotation;

    /**
     * For expansion
     */
    protected CommandValidator() {
        this.rootAnnotation = null;
    }

    protected CommandValidator(Class<? extends Annotation> rootAnnotation) {
        this.rootAnnotation = rootAnnotation;
    }

    /**
     * インスタンスを構築します.
     * <P>
     * 検証対象のルートとなるアノテーションは検査結果のメッセージに使用します.未指定（{@code null}）の場合も
     * 対象となったアノテーションが不明なだけでメッセージの出力はできます.
     *
     * @param rootAnnotation 検証対象のルートとなるアノテーション.
     * @return 構築したインスタンス
     */
    public static CommandValidator of(Class<? extends Annotation> rootAnnotation) {
        return new CommandValidator(rootAnnotation);
    }

    /**
     * 検証を実行します.
     *
     * @param validateTarget 検証を実施する対象
     * @throws AnnotationProcessorException 検証結果に不正があった場合
     */
    public void validate(ValidationInterface... validateTarget) {
        ValidationResult result = ValidationResult.create();
        for (ValidationInterface target : validateTarget) {
            result.append(target.validate());
        }
        if (result.isValid() == false) {
            String message = this.rootAnnotation != null
                             ? this.rootAnnotation.getSimpleName() + " annotated item validation is invalid."
                             : "proccesor command validation result is invalid .";
            throw new AnnotationProcessorException(result, message);
        }
    }
}
