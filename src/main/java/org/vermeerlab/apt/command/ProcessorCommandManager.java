/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.apt.command;

import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;
import org.vermeerlab.apt.config.CommandConfigScannerHandler;
import org.vermeerlab.apt.config.ConfigXmlReader;
import org.vermeerlab.apt.config.RootCommandConfigScanner;

/**
 * {@literal Annotation Processor}で実行するコマンドクラスを管理します.
 *
 * @see org.vermeerlab.apt.command.ProcessorCommandInterface
 * @see org.vermeerlab.apt.command.PostProcessorCommandInterface
 *
 * @author Yamashita,Takahiro
 */
public class ProcessorCommandManager {

    Set<ProcessorCommandInterface> processorCommands;

    /**
     * for expansion
     */
    protected ProcessorCommandManager() {
        this(Collections.emptySet());
    }

    protected ProcessorCommandManager(Set<ProcessorCommandInterface> commands) {
        this.processorCommands = Collections.unmodifiableSet(commands);
    }

    /**
     * インスタンスを構築します.
     *
     * @param processorCommandXmlPath Processor実行環境の情報のパス
     * @return 構築したインスタンス
     */
    public static ProcessorCommandManager of(String processorCommandXmlPath) {
        ConfigXmlReader configXmlReader = ConfigXmlReader.of(processorCommandXmlPath);

        RootCommandConfigScanner rootCommandConfigScanner
                                 = new RootCommandConfigScanner().readConfig(configXmlReader);

        Set<String> commandNames = rootCommandConfigScanner.getValidCommandNames();

        Set<ProcessorCommandInterface> processorCommands
                                       = CommandConfigScannerHandler.of(configXmlReader, commandNames).createCommandInstances();

        rootCommandConfigScanner.displayCommandInfo(processorCommands);
        return new ProcessorCommandManager(processorCommands);
    }

    /**
     * コマンドインスタンスのセットを返却します.
     *
     * @return コマンドインスタンスのセット
     */
    public Set<ProcessorCommandInterface> commands() {
        return this.processorCommands;
    }

    /**
     * 最終ラウンドで実行するコマンドインスタンスのセットを返却します.
     *
     * @return 最終ラウンドで実行するコマンドインスタンスのセット
     */
    public Set<PostProcessorCommandInterface> postProcess() {
        return this.processorCommands.stream()
                .filter(command -> command instanceof PostProcessorCommandInterface)
                .map(PostProcessorCommandInterface.class::cast)
                .collect(Collectors.toSet());
    }
}
