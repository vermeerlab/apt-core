/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
/**
 * {@literal AnnotationProcessor} から実行するコマンドを管理します.
 * <P>
 * {@literal AnnotationProcessor} とコマンドの依存関係を循環させないようにするためのパッケージです.
 * <P>
 * 検証をしたい場合は、{@link org.vermeerlab.apt.command.ValidationInterface} を実装して
 * {@link org.vermeerlab.apt.command.CommandValidator} を使用すれば一括して検証することができます.
 * 例えば、各Element分類毎に検証をしたい場合は、対象Elementを扱うクラスに {@code impliment} しておき
 * {@code CommandValidator} で検証することを想定しています.
 *
 */
package org.vermeerlab.apt.command;
