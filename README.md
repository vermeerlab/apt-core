Pluggable Annotation Processing API Core
===========================================================

`Pluggable Annotation Processing API`を実行する基底となるプロジェクトです。

## Description

以下のインターフェースを実装することで、`Pluggable Annotation Processing API`の処理対象となります。

* 要素毎に処理を行う場合
```java
org.vermeerlab.apt.command.ProcessorCommandInterface
```

* 要素毎に行った情報をラウンドの最後で処理したい場合
```java
org.vermeerlab.apt.command.PostProcessorCommandInterface
```

## Requirement
事前にローカルリポジトリに `parentpom` プロジェクトを作成してください。

`git clone https://bitbucket.org/vermeerlab/parentpom.git`

## Usage
Maven Repository

https://github.com/vermeerlab/maven/tree/mvn-repo/org/vermeerlab/annotation-processor-core

## LICENSE
Licensed under the [Apache License, Version 2.0]({http://www.apache.org/licenses/LICENSE-2.0)

## Author
[_vermeer_](https://twitter.com/_vermeer_)
